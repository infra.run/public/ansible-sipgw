sip-gw
======

Sets up a freeswitch-based host that will forward calls do a cluster node based on a number scheme

Dependencies
------------

This role does not depend on other roles. However, the bigbluebutton-tiny role depends on this role for cluster-based dial-in.

Because freeswitch is choosy, this role only works on Debian 10 for now.

License
-------

BSD
